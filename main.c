#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>

int make_memory_executable(void* m, size_t size);
int make_memory_writeable(void* m, size_t size);
void* alloc_writable_memory(size_t size);
int test();
void restor_caller_saved_registers(unsigned char * mem, int * pos);
void save_caller_saved_registers(unsigned char * mem, int * pos);
void call(unsigned char * mem, int * pos, void * func);
void create_int_test(unsigned char * cm);
void create_long_test(unsigned char * cm);

void print_hex_memory(void *mem) {
  int i;
  unsigned char *p = (unsigned char *)mem;
  for (i=0;i<128;i++) {
    printf("0x%02x ", p[i]);
    if (i%16==15)
      printf("\n");
  }
  printf("\n");
}


int main(int argc, char **argv)
{
	long page_size = sysconf(_SC_PAGESIZE);
	printf("page size is %ld\n\r",page_size);
	
	size_t SIZE = page_size*sizeof(unsigned char);
		
	void* m = alloc_writable_memory(SIZE);
	unsigned char * cm = (unsigned char *)m;
	
	create_long_test(cm);	
	print_hex_memory(m);

	//call(m,&pos,test);
	make_memory_executable(m, SIZE);

	long (*func)() = m;
	long result = func();
	munmap(m,SIZE);
	printf("result = %ld\n", result);
	printf("address of the function is %ld or %ld \n", test, &test);
	getchar();
	return 0;
}

int test(){
	return 42;
}


void create_long_test(unsigned char * cm){
		
	long num = 0x123456789abcdef;
	
	int a = 0;
	int * pos;
	pos = &a;

	cm[*pos] = 0x48;
	*pos += 1;
	cm[*pos] = 0xb8;
	*pos += 1;	
	memcpy(&cm[*pos], &num, 8);
	*pos += 8;
	cm[*pos] = 0xc3;	
}

void create_int_test(unsigned char * cm){
		
	int num = 17;
	
	int a = 0;
	int * pos;
	pos = &a;

	cm[*pos] = 0xb8;
	*pos += 1;
		
	memcpy(&cm[*pos], &num, 4);
	*pos += 4;
	cm[*pos] = 0xc3;
}

//make a absolute indirect call to func
void call(unsigned char * mem, int * pos, void * func){
	//mov func to rax	
	// mov %rax,<constant>
	mem[(*pos)++] = 0x48;
	mem[(*pos)++] = 0xb8;	
	
	//write the constant, little endian
	long ifunc = (long)func;
	memcpy(mem, &ifunc, 8);
	*pos = *pos + 8;
				
	//mem[(*pos)++] = 0xff; //callq  *%rax
	//mem[(*pos)++] = 0xd0; //ff d + #reg
}

//pushes RAX, RCX and RDX to stack
void save_caller_saved_registers(unsigned char * mem, int * pos){
	mem[(*pos)++] = 0x50; //push rax
	mem[(*pos)++] = 0x51; //push rcx
	mem[(*pos)++] = 0x52;	//push rdx
}

//restores rax, rcx and rdx from stack
void restor_caller_saved_registers(unsigned char * mem, int * pos){
	mem[(*pos)++] = 0x58; //pop to rax
	mem[(*pos)++] = 0x59; //pop to rcx
	mem[(*pos)++] = 0x5A;	//pop to rdx
}


// Allocates RW memory of given size and returns a pointer to it. On failure,
// prints out the error and returns NULL. Unlike malloc, the memory is allocated
// on a page boundary so it's suitable for calling mprotect.
void* alloc_writable_memory(size_t size) {
  void* ptr = mmap(0, size,
                   PROT_READ | PROT_WRITE,
                   MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (ptr == (void*)-1) {
    perror("mmap");
    return NULL;
  }
  return ptr;
}

// Sets a RX permission on the given memory, which must be page-aligned. Returns
// 1 on success. On failure, prints out the error and returns 0.
int make_memory_executable(void* m, size_t size) {
  if (mprotect(m, size, PROT_READ | PROT_EXEC) == -1) {
    perror("mprotect, make executable");
    return 0;
  }
  return 1;
}



// Sets a WX permission on the given memory. Returns
// 1 on success. On failure, prints out the error and returns 0.
int make_memory_writeable(void* m, size_t size) {
  if (mprotect(m, size, PROT_READ | PROT_WRITE) == -1) {
    perror("mprotect, make writeable");
    return 0;
  }
  return 1;
}